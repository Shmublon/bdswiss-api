export interface IUser {
  firstName: string;
  lastName: string;
  email: string;
  passwordHash: string;
  id?: string | number;
}
