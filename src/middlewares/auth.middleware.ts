/* eslint-disable @typescript-eslint/no-explicit-any */
import HttpStatus from 'http-status-codes';
import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import { config } from 'dotenv';
config();

/**
 * Middleware to authenticate if user has a valid Authorization token
 * Authorization: Bearer <token>
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
export const userAuth = async (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    const bearerToken = req.cookies.jwt;
    if (!bearerToken)
      throw {
        code: HttpStatus.BAD_REQUEST,
        message: 'Authorization token is required'
      };

    console.log(bearerToken);
    const { user }: any = await jwt.verify(
      bearerToken,
      process.env.ACCESS_TOKEN_SECRET
    );
    console.log(user);
    res.locals.user = user;
    res.locals.token = bearerToken;
    next();
  } catch (error) {
    console.error(error);
    next(error);
  }
};
